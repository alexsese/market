<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Market
 *
 * @author a073659633v
 */
class Market extends CI_Model{
    public function get_producto() {
//El primer paso es escribir la consulta y guardarla en la variable $sql
        $sql = <<< SQL
            SELECT * 
              FROM producto
SQL;
//Ejecutar la consulta
        $consulta = $this->db->query($sql);
//Pasar los resultados al controlador
        return $consulta->result();
    }
}
